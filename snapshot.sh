#!/bin/bash
# depends: ffmpeg, ffplay

# Open an ffplay window playing a stream from the specified camera,
# and upon closing it, take a picture from that camera
# and saving it to an incrementally-named file.

# Takes one argument: camera device to use
# Defaults to using /dev/video0

CAMERA=${1:-/dev/video0}

if [[ -f "photo-0.jpg" ]] ; then
  FILENAME="photo-$(($(find . -maxdepth 1 -type f -name 'photo-*.jpg' |\
                          sort |\
                          tail -n 1 |\
                          grep -o '[0-9]*') + 1 )).jpg"
else
  FILENAME="photo-0.jpg"
fi


ffplay \
  -fflags nobuffer \
  -flags low_delay \
  -framedrop \
  "${CAMERA}" \
  >/dev/null 2>&1

ffmpeg \
  -hide_banner \
  -f video4linux2 \
  -i "${CAMERA}" \
  -vframes 1 \
  "${FILENAME}" \
  -n \
  >/dev/null 2>&1

echo -n "${FILENAME}"
