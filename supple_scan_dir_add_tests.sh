#!/bin/bash

# Specific to Supple development

rm -f CMakeLists.txt

for FILE in $(find . -maxdepth 1 -type f -name "*.cpp" | sort | sed 's|^\./||'); do
  echo supple_add_test\("\${CMAKE_CURRENT_SOURCE_DIR}/${FILE}"\) >> CMakeLists.txt
done

echo >> CMakeLists.txt

for DIR in $(find . -maxdepth 1 -type d | sort | grep -v '^\.$' | sed 's|^\./||'); do
  echo add_subdirectory\("\${CMAKE_CURRENT_SOURCE_DIR}/${DIR}"\) >> CMakeLists.txt
done
