#!/bin/bash

function mklambda() {
    file=$(mktemp)
    printf "#!/bin/bash\n\n" > ${file}
    printf "${*}" >> ${file}
    chmod +x ${file}
    printf ${file}
}

mylambda=$(mklambda 'echo $1\n' 'echo $1')

${mylambda} Hello
${mylambda} Lambdas
