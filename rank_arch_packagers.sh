#!/bin/bash

package_list_command="pacman -Qq"

while [ ! -z ${1} ]; do
  case "${1}" in
    "--native"|"-n")
      shift
      package_list_command="pacman -Qqn"
      ;;
    "--foreign"|"-f")
      shift
      package_list_command="pacman -Qqm"
      ;;
    "--all"|"-a")
      shift
      ;;
    "--command"|"-c")
      shift
      package_list_command="${1}"
      shift
      ;;
    "--search"|"-s")
      shift
      package_list_command="pacman -Qqs ${1}"
      shift
      ;;
    "--help"|"-h")
      shift
      echo -e "rank_arch_packagers.sh"
      echo -e "Options:"
      echo -e "\t-n  --native\t\t\t rank for only main repository packages"
      echo -e "\t-f  --foreign\t\t\t rank for only foreign (not main repository) packages"
      echo -e "\t-a  --all\t\t\t rank for all packages [default]"
      echo -e "\t-c  --command \"<command>\"\t use custom pacman list command (e.g. -c \"pacman -Qqe\" for only packages installed explicitly)"
      echo -e "\t-s  --search <query>\t\t rank only packages that match the search query"
      echo -e "\t-h  --help\t\t\t print this help message"
      exit 0
      ;;
    *)
      echo "Unknown Option: " ${1}
      exit 1
      ;;
  esac
done

pacman -Qi $(eval ${package_list_command}) | \
 grep -i packager                          | \
 cut -d : -f 2                             | \
 sed 's/ <.*>//'                           | \
 sort                                      | \
 uniq -c                                   | \
 sed 's/^\s\+//'                           | \
 sed 's/^\s*\([0-9]\{1\}\) /0\1/'          | \
 sed 's/^\s*\([0-9]\{2\}\) /0\1/'          | \
 sed 's/^\s*\([0-9]\{3\}\)\s*/\1   /'      | \
 sort -r
