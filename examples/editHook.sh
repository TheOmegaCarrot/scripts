#!/bin/bash

# argument 1: file to monitor changes to
# argument 2: 

# validate argument is a file
if [ -e "${1}" ]; then
	FILE="${1}"
else
	exit
fi

COMMAND="${2}"
HASHDIR="/tmp/editHook"

if [ ! -d "${HASHDIR}" ]; then
	mkdir "${HASHDIR}"
fi

INITHASH="$(sha1sum ${FILE} | awk '{print $1}')"

echo "${INITHASH}" > "${HASHDIR}/${FILE}-${INITHASH}"

while true; do
	HASH="$(sha1sum ${FILE} | awk '{print $1}')"
	if [ ! "${HASH}" = "$(cat ${HASHDIR}/${FILE}-${INITHASH})" ]; then
		echo "Changed"
		echo "${HASH}" > "${HASHDIR}/${FILE}-${INITHASH}"
		${COMMAND}
	else
		echo "Unchanged"
	fi

	sleep 2

done
