#!/bin/bash

declare -a CHILDARR
for NAME in $(find . -follow -maxdepth 1 -type d | grep '/' | sed 's/\.\///'); do
  if [ -d "${NAME}/.git" ]; then
    git -C ${NAME} pull&
    CHILDARR+=($!)
  fi
done

for CHILD in "${CHILDARR[@]}"; do
  wait ${CHILD}
done
