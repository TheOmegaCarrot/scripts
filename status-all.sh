#!/bin/bash

for NAME in $(find . -follow -maxdepth 1 -type d | grep '/' | sed 's/\.\///'); do
  if [ -d "${NAME}/.git" ]; then
    echo "In ${NAME}"
    git -C ${NAME} status
    echo
    echo
  fi
done
