#!/bin/bash

if ( expr "${1}" : "[yY].*" > /dev/null ); then
	echo "Yes"
else
	echo "No"
fi
