#!/bin/bash

# usage: enhance.sh INFILE OUTFILE LEVEL

INFILE=${1}
OUTFILE=${2}
COUNT=${3}

if [ -z ${1} ]; then
  echo "No arguments supplied"
  echo "usage: enhance.sh INFILE OUTFILE LEVEL"
  exit
fi

if [ -z ${2} ]; then
  echo "Not enough arguments supplied"
  echo "usage: enhance.sh INFILE OUTFILE LEVEL"
  exit
fi

if [ -z ${3} ]; then
  echo "Not enough arguments supplied"
  echo "usage: enhance.sh INFILE OUTFILE LEVEL"
  exit
fi

if [ ! -f "${INFILE}" ]; then
  echo "Input file \"${INFILE}\" invalid"
  echo "usage: enhance.sh INFILE OUTFILE LEVEL"
  exit
fi

if [ -f "${OUTFILE}" ]; then
  echo "Output file \"${OUTFILE}\" already exists."
  read -r -p "Clobber file? [y/N] " input
  case ${input} in
    [Yy][Ee][Ss]|[Yy])
      echo "Clobbering..."
      ;;
    [Nn][Oo]|[Nn])
      echo "Not clobbering, exiting"
      exit
      ;;
    *)
      echo "Not clobbering, exiting"
      exit
      ;;
  esac

fi

if [ "${COUNT}" -le 0 ]; then
  echo "Invalid enhancement level \"${COUNT}\". Must be an integer greater than 0"
  echo "usage: enhance.sh INFILE OUTFILE LEVEL"
  exit
fi


CMD="convert \"${INFILE}\""

for NUM in $(seq ${COUNT}); do
  CMD="${CMD} -enhance"
done

CMD="${CMD} \"${OUTFILE}\""

# echo ${CMD}

eval -- ${CMD}
