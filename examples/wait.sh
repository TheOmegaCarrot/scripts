#!/bin/bash

declare -a CHILDARR
for N in $(seq 1 3 10); do
  sleep $((N + 10))&
  CHILDARR+=($!)
done

for CHILD in "${CHILDARR[@]}"; do
  echo waiting for ${CHILD}
  wait ${CHILD}
done

echo done
