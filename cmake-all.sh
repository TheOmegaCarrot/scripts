#!/bin/bash

# Compile and test with GCC and Clang
# in both debug and release configuration
# building against C++17, C++20, and C++23

test_compiler() {
  COMPILER=${1}
  NAME=${2}
  shift
  shift
  FLAGS=${@}

  if [ -x ${PREFIX}/${COMPILER} ]; then

    cat << EOF

##############################################################################
                              ${NAME} DEBUG
##############################################################################

EOF

    if [ ! -d ${BINDIR}/${NAME}-debug ]; then
      cmake -S ${SRCDIR} -B ${BINDIR}/${NAME}-debug -DCMAKE_BUILD_TYPE=Debug -D${SUPPLE_}SANITIZE_DEBUG=NO -D${SUPPLE_}FULL_TESTS=YES ${FLAGS} -DCMAKE_CXX_COMPILER=${COMPILER} -G Ninja
      echo
    fi
    cmake --build ${BINDIR}/${NAME}-debug --parallel ${NUM_THREADS}
    cmake --build ${BINDIR}/${NAME}-debug --parallel ${NUM_THREADS} --target test

    cat << EOF

##############################################################################
                               ${NAME} RELEASE
##############################################################################

EOF


    if [ ! -d ${BINDIR}/${NAME}-release ]; then
      cmake -S ${SRCDIR} -B ${BINDIR}/${NAME}-release -DCMAKE_BUILD_TYPE=Release -D${SUPPLE_}FULL_TESTS=Yes ${FLAGS} -DCMAKE_CXX_COMPILER=${COMPILER} -G Ninja
      echo
    fi
    echo
    cmake --build ${BINDIR}/${NAME}-release --parallel ${NUM_THREADS}
    cmake --build ${BINDIR}/${NAME}-release --parallel ${NUM_THREADS} --target test

  else

    echo "${COMPILER}" not found

  fi

}

declare -a MOREARGS

SUPPLE_=""
NUM_THREADS="$(nproc)"

while [ ! -z ${1} ]; do

  case ${1} in
    "-S")
      shift
      SRCDIR=${1}
      shift
      ;;
    "-B")
      shift
      BINDIR=${1}
      shift
      ;;
    "-j")
      shift
      NUM_THREADS=${1}
      shift
      ;;
    "--supple")
      shift
      SUPPLE_="SUPPLE_"
  esac

done

test_compiler /opt/gcc-9/bin/g++ GCC-9 -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/gcc-9.cmake' -D${SUPPLE_}OMIT_23=YES
test_compiler /opt/gcc-10/bin/g++ GCC-10 -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/gcc-10.cmake' -D${SUPPLE_}OMIT_23=YES
test_compiler /opt/gcc-11/bin/g++ GCC-11 -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/gcc-11.cmake'
test_compiler /opt/gcc-12/bin/g++ GCC-12 -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/gcc-12.cmake'
test_compiler /opt/gcc-13/bin/g++ GCC-13 -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/gcc-13.cmake'
test_compiler /opt/gcc-trunk/bin/g++ GCC-TRUNK -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/gcc-trunk.cmake'
test_compiler /usr/bin/g++ GCC-SYSTEM
test_compiler /opt/llvm-11/bin/clang++ CLANG-11 -D${SUPPLE_}OMIT_23=YES
test_compiler /opt/llvm-11/bin/clang++ CLANG-11-LIBC++ -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/clang-11-libc++.cmake' -D${SUPPLE_}OMIT_23=YES
test_compiler /opt/llvm-12/bin/clang++ CLANG-12
test_compiler /opt/llvm-12/bin/clang++ CLANG-12-LIBC++ -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/clang-12-libc++.cmake'
test_compiler /opt/llvm-13/bin/clang++ CLANG-13
test_compiler /opt/llvm-13/bin/clang++ CLANG-13-LIBC++ -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/clang-13-libc++.cmake'
test_compiler /opt/llvm-14/bin/clang++ CLANG-14
test_compiler /opt/llvm-14/bin/clang++ CLANG-14-LIBC++ -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/clang-14-libc++.cmake'
test_compiler /usr/bin/clang++-15 CLANG-15
test_compiler /opt/llvm-15/bin/clang++ CLANG-15-LIBC++ -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/clang-15-libc++.cmake'
test_compiler /opt/llvm-16/bin/clang++ CLANG-16
test_compiler /opt/llvm-16/bin/clang++ CLANG-16-LIBC++ -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/clang-16-libc++.cmake'
test_compiler /opt/llvm-17/bin/clang++ CLANG-17
test_compiler /opt/llvm-17/bin/clang++ CLANG-17-LIBC++ -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/clang-17-libc++.cmake'
test_compiler /opt/llvm-18/bin/clang++ CLANG-18
test_compiler /opt/llvm-18/bin/clang++ CLANG-18-LIBC++ -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/clang-18-libc++.cmake'
test_compiler /opt/llvm-trunk/bin/clang++ CLANG-TRUNK
test_compiler /opt/llvm-trunk/bin/clang++ CLANG-TRUNK-LIBC++ -DCMAKE_TOOLCHAIN_FILE='/opt/cmake-toolchain-files/clang-trunk-libc++.cmake'
test_compiler /usr/bin/clang++ CLANG-SYSTEM
test_compiler /opt/intel/icpx-2023.0.0/oneapi/compiler/2023.0.0/linux/bin/intel64/icc ICC-2021.8 -D${SUPPLE_}OMIT_23=YES
test_compiler /opt/intel/icpx-2023.0.0/oneapi/compiler/2023.0.0/linux/bin/icpx ICPX-2023.0
test_compiler /opt/intel/icpx-2023.1.0/oneapi/compiler/2023.1.0/linux/bin/intel64/icc ICC-2021.9 -D${SUPPLE_}OMIT_23=YES
test_compiler /opt/intel/icpx-2023.1.0/oneapi/compiler/2023.1.0/linux/bin/icpx ICPX-2023.1
test_compiler /opt/intel/icpx-2023.2.1/oneapi/compiler/2023.2.1/linux/bin/icpx ICPX-2023.2
test_compiler /opt/intel/icpx-2023.2.1/oneapi/compiler/2023.2.1/linux/bin/intel64/icc ICC-2021.10 -D${SUPPLE_}OMIT_23=YES
test_compiler /opt/intel/icpx-2024.0.2/oneapi/compiler/2024.0/bin/icpx ICPX-2024.0.2
