#!/bin/bash

DELAY="0.15"

echo -ne "\n\r\  ${1}"
sleep "${DELAY}"

while true; do
	echo -ne "\r|  ${1}"
	sleep "${DELAY}"
	echo -ne "\r/  ${1}"
	sleep "${DELAY}"
	echo -ne "\r-- ${1}"
	sleep "${DELAY}"
	echo -ne "\r\  ${1}"
	sleep "${DELAY}"
done
