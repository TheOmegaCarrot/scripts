#!/bin/bash

OPACITY=$(echo "0.0
0.1
0.2
0.3
0.4
0.5
0.6
0.7
0.8
0.9
1.0" | dmenu -p "select opacity")

sed -i "s/background_opacity: .*$/background_opacity: $OPACITY/" /home/ethan/.config/alacritty/alacritty.yml
