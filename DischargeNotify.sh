#!/bin/sh

# get battery percentage
getbattery() {
	acpi | head -n 1 | cut -d , -f 2 | tr -d % | tr -d \  
}

# save first digit of battery
getbattery | cut -c 1 > /tmp/batteryfirstdigit

while :
do
	BATTERY="$(getbattery)"
	BATTERYFIRSTDIGIT="$(echo ${BATTERY} | cut -c 1)"

	# notify if battery charge level has crossed a 10% mark (first digit changed)
	if [ "${BATTERYFIRSTDIGIT}" != "$(cat /tmp/batteryfirstdigit)" ]; then
		notify-send --expire-time=7000 "Battery:" "${BATTERY}%"
	fi

	echo ${BATTERYFIRSTDIGIT} > /tmp/batteryfirstdigit

	sleep 60
done
