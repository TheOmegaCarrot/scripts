#!/bin/bash

# Compile and test with GCC and Clang
# in both debug and release configuration
# building against C++17
#
# Purpose-built for my organizational scheme and conventions
#
# TODO: make testing C++20 and C++23 possible

test_toolchain() {
  TOOLCHAIN_FILE=${1}
  NAME=$(sed 's/\.cmake//' <<< "${TOOLCHAIN_FILE}" | xargs basename)
  shift
  FLAGS=${@}

  cat << EOF

##############################################################################
                              ${NAME} DEBUG
##############################################################################

EOF

  if [ ! -d ${BINDIR}/${NAME}-debug ]; then
    cmake -S ${SRCDIR} -B ${BINDIR}/${NAME}-debug -DCMAKE_BUILD_TYPE=Debug -D${SUPPLE_}SANITIZE_DEBUG=NO -D${SUPPLE_}FULL_TESTS=NO ${FLAGS} -DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_FILE} -G Ninja
    echo
  fi
  cmake --build ${BINDIR}/${NAME}-debug --parallel ${NUM_THREADS}
  cmake --build ${BINDIR}/${NAME}-debug --parallel ${NUM_THREADS} --target test

  cat << EOF

##############################################################################
                               ${NAME} RELEASE
##############################################################################

EOF


  if [ ! -d ${BINDIR}/${NAME}-release ]; then
    cmake -S ${SRCDIR} -B ${BINDIR}/${NAME}-release -DCMAKE_BUILD_TYPE=Release -D${SUPPLE_}FULL_TESTS=NO ${FLAGS} -DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_FILE} -G Ninja
    echo
  fi
  echo
  cmake --build ${BINDIR}/${NAME}-release --parallel ${NUM_THREADS}
  cmake --build ${BINDIR}/${NAME}-release --parallel ${NUM_THREADS} --target test

}

SUPPLE_=""
NUM_THREADS="$(nproc)"

while [ ! -z ${1} ]; do

  case ${1} in
    "-S")
      shift
      SRCDIR=${1}
      shift
      ;;
    "-B")
      shift
      BINDIR=${1}
      shift
      ;;
    "-j")
      shift
      NUM_THREADS=${1}
      shift
      ;;
    "--supple")
      shift
      SUPPLE_="SUPPLE_"
  esac

done

for gcc in $(find /opt/compilers/cmake-toolchain-files/gcc/ -name "*.cmake" | sort | grep -v "new"); do
  test_toolchain "${gcc}"
done

for clang in $(find /opt/compilers/cmake-toolchain-files/clang/ -name "*.cmake" | sort | grep -v "new"); do
  test_toolchain "${clang}"
done

