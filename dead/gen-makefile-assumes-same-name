#!/bin/bash
# vim: filetype=sh

[ -e makefile ] && mv makefile makefile.prev
touch makefile

# shellcheck disable=SC1091
[ -e PROJENV ] && source PROJENV
[ -z "$BINDIR" ] && BINDIR="bin"
[ -z "$BUILDDIR" ] && BUILDDIR="obj"
[ -z "$SRCDIR" ] && SRCDIR="src"
[ -z "$HEADDIR" ] && HEADDIR="src/inc"
[ -z "$PROJNAME" ] && PROJNAME=$(pwd | rev | awk -F / '{print $1}' | rev)
[ -z "$COMPILER" ] && COMPILER="g++"
[ -z "$STANDARD" ] && STANDARD="c++17"
[ -z "$OPTIMIZATION" ] && OPTIMIZATION="0"
[ -z "$ADDFLAGS" ] && ADDFLAGS="-Wall -Wextra -pedantic -g"
COMPILE="${COMPILER} ${ADDFLAGS} -std=${STANDARD} \
-O${OPTIMIZATION} -I ${HEADDIR}"
BIN="${BINDIR}/${PROJNAME}"

{
	echo "BINDIR=\"$BINDIR\""
	echo "BUILDDIR=\"$BUILDDIR\""
	echo "SRCDIR=\"$SRCDIR\""
	echo "HEADDIR=\"$HEADDIR\""
	echo "PROJNAME=\"$PROJNAME\""
	echo "COMPILER=\"$COMPILER\""
	echo "STANDARD=\"$STANDARD\""
	echo "OPTIMIZATION=\"$OPTIMIZATION\""
	echo "ADDFLAGS=\"$ADDFLAGS\""
} > PROJENV

[ -d "$BUILDDIR" ] || mkdir "${BUILDDIR}"
[ -d "$BINDIR" ] || mkdir "${BINDIR}"

SOURCEFILES=$(find "${SRCDIR}" -maxdepth 1 -type f -name "*.cpp" | sort)

BASENAMES=$( \
	( sed -e "s|${SRCDIR}/||g" -e 's|.cpp||g' | sort )\
	<<< "${SOURCEFILES}" )

OBJECTFILES=$( \
	( sed -e "s|${SRCDIR}|${BUILDDIR}|g" -e "s|cpp$|o|g" )\
	<<< "${SOURCEFILES}" )

{
	echo -e "all: ${BIN}\n"

	echo -n "${BIN}: "
} >> makefile

for FILE in ${OBJECTFILES};
do
	echo -n "${FILE} " >> makefile
done

{
	echo

	# echo -e "\t${COMPILE} ${BUILDDIR}/*.o -o ${BIN}\n"
	echo -ne "\t${COMPILE} "
} >> makefile

for FILE in ${OBJECTFILES};
do
	echo -n "${FILE} " >> makefile
done

echo -e "-o ${BIN}\n" >> makefile

for FILE in ${BASENAMES};
do
	{
		INCLUDED=$( grep -e "^#include \"[A-Za-z.]*\""\
			"${HEADDIR}/${FILE}.h" |\
			grep -o "\"[A-Za-z.]*\"" |\
			sed -e 's/"//g' -e 's/\.h$//g' )
		echo -n "${BUILDDIR}/${FILE}.o: ${SRCDIR}/${FILE}.cpp ${HEADDIR}/${FILE}.h "
		for INC in ${INCLUDED}; do
			echo -n "${HEADDIR}/${INC}.h " >> makefile
		done
		echo

		echo -e "\t${COMPILE} -c ${SRCDIR}/${FILE}.cpp -o ${BUILDDIR}/${FILE}.o\n"
	} >> makefile
done

{
	echo "clean:"
	echo -e "\trm -f ${BUILDDIR}/*.o ${BINDIR}/*"
} >> makefile

sed -i 's/\s$//' makefile

ctags -R . 2>/dev/null

ln -sf "${BIN}" ./run

make clean >/dev/null
