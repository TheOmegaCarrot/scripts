#!/bin/sh

 ffmpeg -hide_banner -strict 2 -hwaccel auto -i "${1}" \
	 -c:v hevc_nvenc \
	 -rc vbr \
	 -cq 28 \
	 -qmin 28 -qmax 28 \
	 -profile:v main10 \
	 -pix_fmt p010le \
	 -b:v 0K \
	 -c:a libopus \
	 -map 0 "${2}"
