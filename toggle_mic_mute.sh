#!/bin/bash

MICID=$(pulsemixer --list-sources \
          | grep -i "Built-in" \
          | grep -iv "Monitor" \
          | grep -o "ID: [a-zA-Z0-9_-]\+" \
          | sed 's/^ID: //' \
        )

pulsemixer --toggle-mute --id ${MICID}
