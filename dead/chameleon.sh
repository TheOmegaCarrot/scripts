#!/bin/sh

COLOR=$(chameleon)

echo "${COLOR}" | xclip

echo "${COLOR}" | xargs -I {} notify-send "Chameleon:" "{}"
