#!/bin/sh

# determine if charging or discharging
getifcharging() {
	acpi | head -n 1 | cut -d \  -f 3 | tr -d ,
}

getifcharging | cut -c 1 > /tmp/amicharging

# TODO: handle "Unknown" case

while :
do
	STATUS="$(getifcharging)"

	if [ "${STATUS}" != "$(cat /tmp/amicharging)" ]; then
			notify-send --expire-time=3000 "Battery: $(acpi | head -n 1 | cut -d , -f 1 | cut -d \  -f 3)" "$(acpi | head -n 1 | cut -d , -f 2), $(acpi | head -n 1 | cut -d , -f 3 | cut -d \  -f 2)"
	fi

	echo ${STATUS} > /tmp/amicharging

	sleep 5
done
